BaseSite = "https://api.telegra.ph/"

class api(str):
	"""
	API Methods for Telegraph
	"""
	createAccount = "createAccount"
	editAccountInfo = "editAccountInfo"
	revokeAccessToken = "revokeAccessToken"
	getPage = "getPage",
	getPageList = "getPageList"
	createPage = "createPage"
	editPage = "editPage"
	deletePage = "deletePage"